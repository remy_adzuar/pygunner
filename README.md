# Projet Py Western Gunner

## Fonctionalités

Le joueur contrôle un personnage dans un espace défini
Un ou Plusieurs Adversaires lui tire dessus
Des objects permettent de se mettre à couvert
La partie est perdue si tout les adversaires meurent
La partie est gagnée si le joueur meurs

## Prévisions

15 min de préparation papier
15 min de préparation PC

12 x 30 min de prototypage
12 x 30 min de peaufinage

## Idées

Plusieurs niveaux, plusieurs ennemis
Pouvoir tirer dans plusieurs directions
Pouvoir détruire le décor
Plusieurs types d'ennemis et de comportement IA
Effet de pluie, tempête de sable, brouillard, foudre
Avoir plusieurs type de gun pour le joueur
Effet de lumière?
Ajout d'animation et de bruitage