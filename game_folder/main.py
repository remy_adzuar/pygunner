import pygame

from config import Config
from state import State

class Main:

    def __init__(self) -> None:
        self.loop = False

        self.config = Config()

        self.state = State(self.config)

        if self.config.get_debug():
            print("INIT MAIN")

    def run(self):
        
        self.loop = True
        while self.loop:
            state = self.state.get_state()
            if state == "MENU":
                self.state.menu()
            if state == "GAME":
                self.state.game()


if __name__ == "__main__":

    pygame.init()

    M = Main()
    M.run()

