import pygame

from config import Config

class Text:

    def __init__(self, config:Config, pos:list=[0,0], content:str="", center:bool=False) -> None:
        self.config = config
        self.pos = pos
        self.content = content
        self.center = center

        self.tx_color = (255,255,255)
        self.bg_color = (0,0,0)
    
    def set_pos(self,pos):
        if len(pos)==2:
            if isinstance(pos[0], int) and isinstance(pos[1], int):
                self.pos = pos
        
    def set_tx_color(self, color:list):
        if len(color) == 3:
            self.tx_color = (color[0]%256,color[1]%256,color[2]%256)
    
    def set_bg_color(self, color:list):
        if len(color) == 3:
            self.bg_color = (color[0]%256,color[1]%256,color[2]%256)

    def load_text(self):

        font = self.config.get_font()
        rendu = font.render(self.content, True, self.tx_color, self.bg_color)
        self.rect = rendu.get_rect()
        if self.center:
            self.rect = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu
    
    def print_text(self):
        self.load_text()
        screen = self.config.get_screen()
        screen.blit(self.rendu, self.rect)