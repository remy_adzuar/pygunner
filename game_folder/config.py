import pygame

class Config:

    def __init__(self) -> None:
        self.screen = pygame.display.set_mode((1080,720))
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font("freesansbold.ttf", 16)

        self.debug = True

    def get_screen(self):
        return self.screen
    
    def get_clock(self):
        return self.clock
    
    def get_font(self):
        return self.font
    
    def get_debug(self):
        return self.debug