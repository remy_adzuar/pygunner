import pygame

from pygame.sprite import Group

class Entity(pygame.sprite.Sprite):
    """
    Définit les objects mouvants et/ou interactif sur la map
    """
    def __init__(self, *groups: Group) -> None:
        super().__init__(*groups)
        self.images = []
        self.image = None # Image courrante
        self.pos = [0,0]
        self.scale = (32,32) # Taille par defaut des sprites

        self.index = 0
        self.frame_count = 0
        self.frame_update_index = 20 # Changement d'image toute les x frames
        self.rect = None
        self.last_move = None
    
    def set_scale(self, scale):
        """Met à l'échelle selon le scale fournit:
        Scale : list d'entier de longueur 2"""
        if len(scale)==2:
            if isinstance(scale[0], int) and isinstance(scale[1], int):
                self.scale = scale
        else:
            raise TypeError("scale n'est pas une liste de longueur 2 ne contenant que des entiers")

    def set_pos(self, pos=None):
        """Fonction pour mettre a jour la position du sprite sur l'ecran
        Met a jour l'attribut self.rect en premier"""
        self.rect = self.image.get_rect()
        if pos == None:
            self.rect.x = self.pos[0]
            self.rect.y = self.pos[1]
        elif len(pos) == 2:
            self.pos = [pos[0],pos[1]]
            self.rect.x = self.pos[0]
            self.rect.y = self.pos[1]
        return None
    
    def get_pos(self):
        return self.pos
    
    def get_center_pos(self):
        return [self.pos[0]+self.scale[0]//2, self.pos[1]+self.scale[1]//2]
    
    def add_image(self, URL):
        """
        Fonction qui charge l'image dans la liste des images de l'entité
        """
        img = pygame.image.load(URL)
        img = pygame.transform.scale(img, self.scale)
        self.images.append(img)
        if self.image == None:
            self.image = self.images[-1]
            self.set_pos()
    
    def next_image(self):
        self.index += 1
        if self.index >= len(self.images):
            self.index = 0
        self.image = self.images[self.index]
    
    def rotate(self, nb:int=0):
        i = 0
        while i < len(self.images):
            self.images[i] = pygame.transform.rotate(self.images[i], nb)
            i += 1

    def update(self):
    
        self.frame_count += 1
        if self.frame_count == 216000: # 3600 secondes fois 60 frames
            self.frame_count = 0
        
        if self.frame_count%self.frame_update_index == 0:
            self.index += 1
            if self.index >= len(self.images):
                self.index = 0
    
        self.image = self.images[self.index]
        self.set_pos()
    
    def move(self, d_pos=(0,0)):
        """Fonction qui déplace la position de l'entité d'autant que le delta_pos passé en paramètres"""
        self.last_move = [d_pos[0], d_pos[1]]
        self.pos[0] += d_pos[0]
        self.pos[1] += d_pos[1]
    
    def undo_move(self):
        """Fonction qui annule le mouvement en le jouant à l'envers"""
        print(self.last_move)
        if self.last_move != None:
            self.pos[0] += -self.last_move[0]
            self.pos[1] += -self.last_move[1]
            self.last_move = None
    
    def check_collide(self, entity):
        if hasattr(entity, "rect"):
            return self.rect.colliderect(entity.rect)

    def check_border_collide(self):
        collide = False
        if self.pos[0] < 0:
            collide = True
        elif self.pos[1] < 0:
            collide = True
        elif self.pos[0]+self.scale[0] > 1080:
            collide = True
        elif self.pos[1]+self.scale[1] > 720:
            collide = True
        return collide

    def point_collide(self, point):
        if len(point)==2:
            return self.rect.collidepoint(point[0],point[1])

                
    

class Player(Entity):
    """
    Définit le personage contrôlable par le joueur
    """
    def __init__(self, *groups: Group) -> None:
        # self.img = pygame.image.load("./assets/imgs/WG_char1-1.png")
        super().__init__(*groups)
        self.HP = 5
        self.add_image("./assets/imgs/WG_char1-1.png")
        self.shooting_direction = "RIGHT"
    
    def command_process(self, command, group = None):
        """Le joueur ne peut gérer qu'une commande par frame"""
        d_pos = [0,0]
        if command == "UP":
            d_pos[1] -= 2
        elif command == "DOWN":
            d_pos[1] += 2
        elif command == "LEFT":
            d_pos[0] -= 2
        elif command == "RIGHT":
            d_pos[0] += 2
        elif command == "SHOOT":
            if group != None:
                self.event_shoot(group)
        if d_pos[0] != 0 or d_pos[1] != 0:
            self.move(d_pos)

    
    def get_shooting_direction(self):
        if self.shooting_direction == "RIGHT":
            return [4,0]
        elif self.shooting_direction == "LEFT":
            return [-4,0]
        elif self.shooting_direction == "UP":
            return [0,-4]
        elif self.shooting_direction == "DOWN":
            return [0,4]
        else:
            print("ERREUR DIRECTION")
    
    def event_shoot(self, group):
        if self.shooting_direction in ["RIGHT","LEFT","UP","DOWN"]:
            B = Bullet(group)
            B.set_pos(self.get_center_pos())
            B.move([-8,-8])
            if self.shooting_direction == "RIGHT":
                B.move([20,0])
            elif self.shooting_direction == "LEFT":
                B.move([-20,0])
            elif self.shooting_direction == "UP":
                B.move([0,-20])
            elif self.shooting_direction == "DOWN":
                B.move([0,20])
            B.set_direction(self.get_shooting_direction())


    def event_bullet(self):
        self.HP -= 1
        if self.HP == 0:
            self.kill()

class Enemy(Entity):
    """
    Définit un adversaire pour le joueur
    """
    def __init__(self, *groups: Group) -> None:
        super().__init__(*groups)
        self.direction = [1,1]
        self.shooting_direction = ""
        self.HP = 3 # Health Point
        self.add_image("./assets/imgs/WG_adv1-2.png")


    def update(self, target=None):
        self.move_to_target(target)
        super().update()        

    def get_delta_pos(self, entity):
        """Determine la difference en x et y entre lui même et l'entity cible
        """
        if isinstance(entity, (Entity, Player)):
            self_pos = self.get_pos()
            entity_pos = entity.get_pos()
            delta_x = self_pos[0] - entity_pos[0]
            delta_y = self_pos[1] - entity_pos[1]
            # print(delta_x, delta_y)
        return (delta_x, delta_y)
    
    def set_shooting_direction(self, direction):
        if direction in ["RIGHT","UP","DOWN","LEFT"]:
            self.shooting_direction = direction

    def event_shoot(self, group):
        if self.shooting_direction in ["RIGHT","UP","DOWN","LEFT"]:
            print("PIEW")
            B = Bullet(group)
            B.set_entity_origin(self)
            B.set_pos(self.get_center_pos())
            B.move([-8,-8])
            direction = None
            if self.shooting_direction == "RIGHT":
                B.move([20,0])
                direction = [4,0]
            elif self.shooting_direction == "LEFT":
                B.move([-20,0])
                direction = [-4,0]
            elif self.shooting_direction == "UP":
                B.move([0,-20])
                direction = [0,-4]
            elif self.shooting_direction == "DOWN":
                B.move([0,20])
                direction = [0,4]
            if direction != None:
                B.set_direction(direction)
    
    def event_bullet(self):
        self.HP -= 1
        if self.HP == 0:
            self.kill()
    
    def rebound(self):
        last_dir = self.direction
        self.direction = [last_dir[0]*-1, last_dir[1]*-1]
    
    def move_to_target(self, target):
        """Fonction qui définit une direction pour se rapprocher de la cible"""
        if target != None and isinstance(target, Entity):
            reppel = 128
            t_pos = target.get_pos()
            d_pos = self.get_delta_pos(target)
            if d_pos != None:
                if d_pos[0]*d_pos[0] >= d_pos[1]*d_pos[1]:
                    if d_pos[0] > reppel:
                        #Entity a gauche
                        self.move([-1,0])
                    elif d_pos[0] < -reppel:
                        self.move([1,0])
                elif d_pos[0]*d_pos[0] < d_pos[1]*d_pos[1]:
                    if d_pos[1] > reppel:
                        self.move([0,-1])
                    elif d_pos[1] < -reppel:
                        self.move([0,1])



class Bullet(Entity):

    count = 0

    def __init__(self, *groups: Group) -> None:
        super().__init__(*groups)
        self.set_scale((16,16))
        self.direction = [0,0]
        self.AP = 1 # Action Point
        self.__class__.count += 1

        self.entity_origin = None

        self.add_image("./assets/imgs/bullets/WG_bullet1-1.png")
        self.add_image("./assets/imgs/bullets/WG_bullet1-2.png")
        self.add_image("./assets/imgs/bullets/WG_bullet1-3.png")
    
    def update(self):

        print(self.__class__.count, self)
        self.move(self.direction)
        super().update()

    def set_entity_origin(self, entity):
        self.entity_origin = entity

    def get_entity_origin(self):
        return self.entity_origin
    
    def set_direction(self, direction):
        if len(direction)==2:
            self.direction = direction

            if direction[0] == 0 and direction[1] > 0:
                # Direction UP
                self.rotate(270)
            if direction[0] == 0 and direction[1] < 0:
                # Direction DOWN
                self.rotate(90)
            if direction[0] > 0 and direction[1] == 0:
                # Direction RIGHT
                self.rotate(0)
            if direction[0] < 0 and direction[1] == 0:
                # Direction LEFT
                self.rotate(180)

        else:
            raise TypeError("set_direction, l'argument direction n'est pas une liste de deux entiers")
    
    def event_touch(self):
        self.AP -= 1
        if self.AP == 0:
            self.kill()

class Crate(Entity):

    def __init__(self, *groups: Group) -> None:
        super().__init__(*groups)
        self.set_scale([32,32])
        self.state = 0
        self.add_image("./assets/imgs/crate/WG_crate1-1.png")
        self.add_image("./assets/imgs/crate/WG_crate1-2.png")
        self.add_image("./assets/imgs/crate/WG_crate1-3.png")
        self.add_image("./assets/imgs/crate/WG_crate1-4.png")
    
    def update(self):
        # super().update()
        self.set_pos()

    def event_bullet(self):
        self.state += 1
        self.next_image()
        if self.state == len(self.images):
            self.kill()
            print("KILL CRATE")
    
    def event_slide(self, entity):
        """Fonction qui déplace la caisse selon le sens de colision"""
        if self.check_collide(entity):
            s_pos = self.get_center_pos()
            e_pos = entity.get_center_pos()

            d_pos = [s_pos[0]-e_pos[0], s_pos[1]-e_pos[1]]
            sq_pos = [d_pos[0]*d_pos[0],d_pos[1]*d_pos[1]]

            if sq_pos[0]>sq_pos[1]:
                # Axe 0
                if e_pos[0]-s_pos[0]>0:
                    # entity a droite
                    self.move([-1,0])
                else:
                    self.move([1,0])
            elif sq_pos[0]<sq_pos[1]:
                # Axe 1
                if e_pos[1]-s_pos[1]>0:
                    # entity en dessous
                    self.move([0,-1])
                else:
                    self.move([0,1])

if __name__ == "__main__":

    G = pygame.sprite.Group()
    B = Bullet(G)