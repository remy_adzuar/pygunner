import pygame
import math

from config import Config
from entity import *

class Physics:

    def __init__(self, config:Config, entities:dict) -> None:
        self.config = config
        self.entities = entities
        if ("player" not in self.entities.keys() or
            "enemies" not in self.entities.keys() or
            "obstacles" not in self.entities.keys()):
            raise TypeError("entities ne contient pas une des clés!")
        if self.config.get_debug():
            print("INIT PHYSICS")

    def update(self, tick):
        self.check_collide()

        for player in self.entities["player"]:
            self.cursor_direction(player)

        if tick%20 == 0:
            self.player_target()

    def cursor_direction(self, entity):
        """Fonction qui retourne la direction en degres de la souris par rapport au centre
        de l'entity"""

        debug = self.config.get_debug()
        debug = False

        if hasattr(entity, "rect") and hasattr(entity, "shooting_direction"):
            rect = entity.rect
            cursor = pygame.mouse.get_pos()
            center_x = rect.centerx
            center_y = rect.centery
            
            cursor_centered_x = cursor[0] - center_x
            cursor_centered_y = cursor[1] - center_y

            base = cursor_centered_x
            oppose = cursor_centered_y

            base_value = True
            if base<0:
                base_value = False
                base = -base
            oppose_value = True
            if oppose<0:
                oppose_value = False
                oppose = -oppose

            # Possible de faire ce calcul sans calcul d'angle ? à fouiller
            if base != 0:
                angle_relativ = (math.atan(oppose/base)*180)/math.pi

                if base_value:
                    # Souris coté droit au joueur
                    if oppose_value:
                        # Souris au dessous du joueur
                        if debug : print("DROIT, DESSOUS")
                        
                        if angle_relativ < 45:
                            if debug : print("RIGHT")
                            entity.shooting_direction = "RIGHT"
                            return "RIGHT"
                        else:
                            if debug : print("DOWN")
                            entity.shooting_direction = "DOWN"
                            return "DOWN"
                    else:
                        # Souris au dessus du joueur
                        if debug : print("DROIT, DESSUS")
                        if angle_relativ < 45:
                            if debug : print("RIGHT")
                            entity.shooting_direction = "RIGHT"
                            return "RIGHT"
                        else:
                            if debug : print("UP")
                            entity.shooting_direction = "UP"
                            return "UP"
                else:
                    # Souris coté gauche au joueur
                    if oppose_value:
                        # Souris au dessous du joueur
                        if debug : print("GAUCHE, DESSOUS")
                        if angle_relativ < 45:
                            if debug : print("LEFT")
                            entity.shooting_direction = "LEFT"
                            return "LEFT"
                        else:
                            if debug : print("DOWN")
                            entity.shooting_direction = "DOWN"
                            return "DOWN"
                    else:
                        # Souris au dessus du joueur
                        if debug : print("GAUCHE, DESSUS")
                        if angle_relativ < 45:
                            if debug : print("LEFT")
                            entity.shooting_direction = "LEFT"
                            return "LEFT"
                        else:
                            if debug : print("UP")
                            entity.shooting_direction =  "UP"
                            return "UP"

            if base != 0 and debug:
                print(oppose, base, "Ratio ", math.atan(oppose/base)*180/math.pi)
            return None


    def player_target(self):
        """
        Test l'ensemble des enemy et déclenche des évènements de tir si la cible est dans l'axe de tir
        """
        for enemy in self.entities["enemies"]:
            for player in self.entities["player"]:
                if isinstance(enemy, Enemy) and isinstance(player, Player):
                    p_pos = player.get_pos()
                    e_pos = enemy.get_pos()

                    if e_pos[0] > p_pos[0]-16 and e_pos[0] < p_pos[0] +16:
                        # targeted on the y axis
                        print("TEST")
                        if p_pos[1] > e_pos[1]:
                            # player on the bottom
                            enemy.set_shooting_direction("DOWN")
                            enemy.event_shoot(self.entities["obstacles"])

                        elif p_pos[1] < e_pos[1]:
                            enemy.set_shooting_direction("UP")
                            enemy.event_shoot(self.entities["obstacles"])

                    elif p_pos[1] < e_pos[1]+16 and p_pos[1] > e_pos[1]-16:
                        # targeted on the x axis
                        if p_pos[0] > e_pos[0]:
                            # Player on the right
                            enemy.set_shooting_direction("RIGHT")
                            enemy.event_shoot(self.entities["obstacles"])
                        elif p_pos[0] < e_pos[0]:
                            enemy.set_shooting_direction("LEFT")
                            enemy.event_shoot(self.entities["obstacles"])

    def check_collide(self):

        for entity in self.entities["player"]:
            if isinstance(entity, Player):
                if entity.check_border_collide():

                    # Empêche le joueur de sortir de l'écran

                    window_width = self.config.get_screen().get_width()
                    window_height = self.config.get_screen().get_height()

                    width = window_width - entity.scale[0]
                    height = window_height - entity.scale[1]

                    pos = entity.get_pos()
                    if pos[0] < 0:
                        entity.set_pos([0,pos[1]])
                    if pos[0] > width:
                        entity.set_pos([width, pos[1]])
                    if pos[1] < 0:
                        entity.set_pos([pos[0],0])
                    if pos[1] > height:
                        entity.set_pos([pos[0], height])
                    
                    if self.config.get_debug():
                        print("UNDO MOVE PLAYER")
            
                for crate in self.entities["obstacles"]:
                    if isinstance(crate, Crate):
                        if entity.check_collide(crate):
                            print("PLOP")
                            crate.event_slide(entity)

                        
    
        for enemy in self.entities["enemies"]:
            if isinstance(enemy, Enemy):
                for bullet in self.entities["obstacles"]:
                    if isinstance(bullet, Bullet):
                        if enemy.check_collide(bullet):
                            if bullet.get_entity_origin() != enemy:
                                #Trigger Enemy Bullet event
                                enemy.event_bullet()
                                #Trigger Bullet Touch event
                                bullet.event_touch()

                                if self.config.get_debug():
                                    print("ENEMY TOUCH BY BULLET")
                    if isinstance(bullet, Crate):
                        # Colision crate and enemy
                        if enemy.check_collide(bullet):
                            enemy.rebound()
        
        for player in self.entities["player"]:
            if isinstance(player, Player):
                for bullet in self.entities["obstacles"]:
                    if isinstance(bullet, Bullet):
                        if player.check_collide(bullet):
                            #Trigger player bullet event
                            player.event_bullet()
                            #Trigger bullet touch event
                            bullet.event_touch()

        for entity in self.entities["obstacles"]:
            if isinstance(entity, Bullet):
                # Kill les bullet qui sorte du jeu
                if entity.check_border_collide():
                    entity.kill()
                    if self.config.get_debug():
                        print("KILL BULLET")
            
            # Collision bullet / obstacles
            if isinstance(entity, Crate):
                for bullet in self.entities["obstacles"]:
                    if isinstance(bullet, Bullet) and entity.check_collide(bullet):
                        entity.event_bullet()
                        bullet.event_touch()
                        