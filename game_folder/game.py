import pygame
import random

from config import Config
from entity import *
from physics import Physics

class Game:

    def __init__(self, config:Config) -> None:
        self.loop = False
        self.tick = 0
        self.config = config

        if self.config.get_debug():
            print("INIT GAME")

    def up_tick(self):
        self.tick += 1
        if self.tick == 216000:
            self.tick = 0

    def get_tick(self)-> int:
        return self.tick

    def run(self):
        self.loop = True
        self.tick = 0

        self.load()

        clock = self.config.get_clock()

        while self.loop:
            self.input_process()

            self.update()
            self.physics.update(self.tick)

            self.draw()

            self.up_tick()
            clock.tick(60)

    def load(self):
        self.entities = {
            "player": pygame.sprite.Group(),
            "enemies": pygame.sprite.Group(),
            "obstacles": pygame.sprite.Group()
        }
        self.player = Player(self.entities["player"])
        enemy1 = Enemy(self.entities["enemies"])
        enemy1.set_pos([random.randrange(0,1080-32),random.randrange(0,720-32)])

        self.crate_test = Crate(self.entities["obstacles"])
        self.crate_test.set_pos([128,128])
        self.map = None



        self.physics = Physics(self.config, self.entities)

    def input_process(self):

        # If ou Elif selon le comportement désiré
        # Elif empêche les déplacement en diagonale
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_w]:
            self.player.command_process("UP")
        if pressed[pygame.K_s]:
            self.player.command_process("DOWN")
        if pressed[pygame.K_a]:
            self.player.command_process("LEFT")
        if pressed[pygame.K_d]:
            self.player.command_process("RIGHT")

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.loop = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: #clic gauche
                    self.player.command_process("SHOOT", self.entities["obstacles"])
            
            if self.config.get_debug():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        print("PLOP")
                        print(self.config.get_screen())
                        print(self.entities["enemies"])
                        self.create_enemy()

    def update(self):

        if len(self.entities["enemies"]) == 0:
            E = Enemy(self.entities["enemies"])
            E.set_pos([random.randrange(0,1080-32),random.randrange(0,720-32)])

        self.entities["obstacles"].update()
        self.entities["enemies"].update(self.player)
        self.entities["player"].update()

    def draw(self):
        screen = self.config.get_screen()
        screen.fill((0,0,0))

        # Process
        # Drawing all sprites
        self.entities["obstacles"].draw(screen)
        self.entities["enemies"].draw(screen)
        self.entities["player"].draw(screen)

        pygame.display.update()
    
    def create_enemy(self):
        """Fonction qui positionne un nouvel enemy sur un point de spawn aléatoire dans les bande sud, nord, est ou ouest"""
        area = random.randint(0,3)
        E = Enemy(self.entities["enemies"])
        if area == 0:
            # Bande Nord
            E.set_pos([random.randrange(0,1080-32), 32])
        elif area == 1:
            # Bande Sud
            E.set_pos([random.randrange(0,1080-32),720-64])
        elif area == 2:
            # Bande Est
            E.set_pos([32,random.randrange(0,720-32)])
        elif area == 3:
            # Brande Ouest
            E.set_pos([720-64, random.randrange(0,720-32)])
