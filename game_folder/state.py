from config import Config
from game import Game

class State:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.state = "GAME"

        if self.config.get_debug():
            print("INIT STATE")
    

    def get_state(self):
        return self.state

    def menu(self):
        pass

    def game(self):
        self._game = Game(self.config)
        self._game.run()
        pass