# Suivis de projet

## Préparation :

15 min préparation papier,
15 min préparation PC,

## Prototypage

10/01/2023
30 min, Mise en place squelette application
Fin de session

30 min, Début implémentation logique des sprites
15 min pause,
30 min, Implémentation logique des sprites
15 min pause,
30 min, Ajout mise à jour des positions de sprites
15 min pause,
30 min, Ajout Clock, Ajout fonction move, ajout des update entities dans Game
15 min pause,
30 min, Ajout déplacement du joueur
Fin de session

30 min, Ajout classe Physics, Ajout mouvement bullet
Fin de session

11/01/2023
30 min, Position de la bullet et début direction du tir

16/01/2023
30 min, Test calcul angle de tir
15 min pause,
30 min, Calcul Angle OK, ajout print direction projectile